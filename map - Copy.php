<?php 
	include 'koneksi.php';
	include 'function.php';
	
//	print_r(getCategory($conn));
	//return;
	
	if(isset($_GET['category_id'])){
		$category_id = $_GET['category_id'];
	} else {
		$category_id = '0';
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <!-- This stylesheet contains specific styles for displaying the map
         on this page. Replace it with your own styles as described in the
         documentation:
         https://developers.google.com/maps/documentation/javascript/tutorial -->
     <link rel="stylesheet" href="style.css"/>
	<style>
	html,body {
		height: 100%;
		margin:0;
		padding:0;
	}
	#map {
	height:100%;
	}
	</style>
  </head>
  <body>
<!--  	disini nanti map akan ditampilkan-->
    <div id="map"></div>
    
<!--    MENU KANAN-->
	<div class="floating-panel">
		<h4>Kategori Tempat</h4>
        <select class="lebarCategory" id="category">
        	<option value="0">Semua</option>
        <?php
		$place_category = getCategory($conn);
		foreach($place_category as $value){
			$auto_select = autoSelect($category_id, $value[id]);
			
			echo "<option value='$value[id]' $auto_select>$value[title]</option>";
		}
		?>
         </select>
         <h4>Tempat</h4>
         <ul>
         	<div id="place_list"></div>
            <?php
				$places = getPlaces($conn, $category_id);

				// if(is_null($places)){
				// 	echo "<option value=''>-- kosong --</option>";
				// } else {
				// 	// looping
				// 	foreach($places as $place){
				// 		echo "<li>$place[name]</li>";
				// 	}
				// }
			?>
         </ul>
         Belajar Web :
         <ul>
         	<li>Click Marker Info Data</li>
            <li>Search Nama Tempat</li>
            <li>Jarak A ke B</li>
            <li>Area Polygon</li>
            <li>Semua Dipilih tanpa dikosongkan</li>
         </ul>
	</div>
    <script>
    var locations = [
      //['North Jakarta', -6.138377, 106.866466],
	  <?php
	  	if(!is_null($places)){
			// looping
			foreach($places as $place){
				// print array buat javascript
				echo "['$place[name]', $place[latitude], $place[longitude]],";

			}
		}
	  ?>
    ];

    function initMap() {
    	console.log(locations);
	  //var locations = {lat: -6.175110, lng: 106.865039};

		// Nah ini proses inisialisasi Mapnya.
		// dia menandai tag html dengan id MAP
        var map = new google.maps.Map(document.getElementById('map'), {
			// bagian ini maksudnya, map akan berada di tengah2 dari koordinat yg kita tentukan dibawah
			// coba kita ganti koordinatnyaya
//          center: {lat: -6.175110, lng: 106.865039}, // zoom ke jakarta
          zoom: 12
        });
		
		// Buat latlong Bound untuk auto fit tampilan map
		var bounds = new google.maps.LatLngBounds();
		var infowindow = new google.maps.InfoWindow();
  	//looping membuat marker
	  var marker, i; 
      for (i = 0; i < locations.length; i++) {  
	  		// kode untuk membuat marker
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map
		  });
		  // extend koordinat ke bound variable
		  bounds.extend(marker.position);
		  // ketika marker di klik
		  google.maps.event.addListener(marker, 'click', (function(marker, i){
		  	return function(){
				//mengambil nama pada locationn array di mulai dari 0
				infowindow.setContent(locations[i][0]);
				infowindow.open(map, marker);
				
				// tampilkan di log
				console.log(locations[i]);
			}
		  })(marker, i));
  
		}
		// Zoom tampilan berada di tengah2 semua marker
		map.fitBounds(bounds);
      }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQWti4OcdJItjY_w8yJHULMGCgO-Nq8_Y&callback=initMap"
    async defer></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
		$(document).ready(function(){
			// event onChange
			$('#category').change(function(event){
				// tampilin pesan di log 
				console.log("diubah");
				var category_id = $(this).val();
				
				// redirect ke map.php dngan bawa parameter get
				//window.location.href = 'map.php?category_id=' + category_id;
				var api_url = "api/get-places.php?category_id=" + category_id;
				$.get(api_url, function( data ) {
				  	var places = data.data;
				  	// kosongkan element dalam tag div id place_list
				    $('#place_list').html('');

				    //console.log(data);
				    // kosongkan dulu variable locaitons
				    locations = [];
				    // cek jika status sukses
				    if (data.isSuccess) {
				    	// looping data places
					  	$.each(places, function(k, place) {
						  	var html_dom = '<li>' + place.name + '</li>'
					        console.log(html_dom);
					        // append ke div
					        $('#place_list').append(html_dom);
					        // push data ke array locations
					        locations.push([place.name, place.latitude, place.longitude]);
					    });
				    } else {
				    	// jika nilai isSuccessnya false
				    	$('#place_list').append('<li> -- kosong -- </li>');
				    }

				    //console.log(locations);
				  	// Jalankan funtion initMap lagi, gunanya untuk merefresh Map
				  	initMap();
				});
			});
		});
	</script>
  </body>
</html>