<?php
function getTypePlace($conn){
	// buat query
	$query = $conn->query("SELECT * FROM type_place");
	
	// variable dummy array
	$result = null;
	while($data = $query->fetch_assoc()){
		// put data ke array 
		$result[] = $data;
	}
	
	// kembalikand atanya
	return $result;
}

function getCategory($conn){
	// buat query
	$query = $conn->query("SELECT * FROM category ORDER BY title ASC");
	
	// variable dummy array
	$result = null;
	while($data = $query->fetch_assoc()){
		// put data ke array 
		$result[] = $data;
	}
	
	// kembalikand atanya
	return $result;
}

function getPlaces($conn, $category_id){
	if($category_id == '0'){
		// select semua data place
		$query = $conn->query("SELECT * FROM places ORDER BY name");
	} else {
		// select berdasarkan id category
		$query = $conn->query("SELECT * FROM places WHERE category_id = '$category_id' ORDER BY name");
		
//		return "SELECT * FROM places WHERE category_id = '$category_id' ORDER BY name";
	}
	// 
	$result = null;
	while($data = $query->fetch_assoc()){
		// put ke variable result 
		$result[] = $data;
	}
	
	// kembalikan
	return $result;
	
}

function getSearchDetail($conn, $place_id){
	if($place_id == '0'){
		// select semua data place
		$query = $conn->query("SELECT * FROM places ORDER BY name");
	} else {
		// select berdasarkan id category
		$query = $conn->query("SELECT * FROM places WHERE id = '$place_id' ORDER BY name");
		
//		return "SELECT * FROM places WHERE category_id = '$category_id' ORDER BY name";
	}
	// 
	$result = null;
	while($data = $query->fetch_assoc()){
		// put ke variable result 
		$result[] = $data;
	}
	
	// kembalikan
	return $result;
	
}


function searchPlace($conn, $keyword){
	// select berdasarkan id category
	$query = $conn->query("SELECT places.*,category.title FROM places,category WHERE category.id=places.category_id AND places.name LIKE '%$keyword%' ORDER BY places.name");
		
	// 
	$result = null;
	while($data = $query->fetch_assoc()){
		// put ke variable result 
		$result[] = $data;
	}
	
	// kembalikan
	return $result;
	
}


function autoSelect($x, $y){
	// cek jika nilai sama
	if($x == $y ){
		$result = "SELECTED";
	} else {
		$result = "";
	}
	
	return $result;
}


function getDetailPlace($conn, $latitude, $longitude){
	// select berdasarkan id category
	$query = $conn->query("SELECT * FROM places WHERE latitude = '$latitude' AND longitude = '$longitude' LIMIT 1");
	// 
	$result = $query->fetch_assoc();
	
	// kembalikan
	return $result;
}
function getDetailPlaceFromId($conn, $id){
	// select berdasarkan id category
	$query = $conn->query("SELECT * FROM places WHERE id = '$id' LIMIT 1");
	// 
	$result = $query->fetch_assoc();
	
	// kembalikan
	return $result;
}

function insertPlace($conn, $request, $files){
	$category_id = $request['category_id'];
	$name = $request['name'];
	$latitude = $request['latitude'];
	$longitude = $request['longitude'];
	$street_address = $request['street_address'];
	$phone = $request['phone'];
	//$photo = $request['photo'];
	$type = $request['type'];
	// Handle image // kita offkan dulu
	// Handle image // kita offkan dulu
	$photo = $files['photo']['tmp_name']; //dapetin nama file
	$photo_name = $files['photo']['name']; //dapetin nama file
	$file_path = basename($files['photo']['name']);
	move_uploaded_file($photo, "../upload/" . $file_path);
	// select berdasarkan id category
	$query = $conn->query("INSERT INTO places (category_id, name, latitude, longitude, street_address, phone, photo, type)
			VALUES ('$category_id', '$name', '$latitude', '$longitude', '$street_address', '$phone', '$photo_name ', '$type') ");
	// 
	$result = $query;
	// kembalikan
	return $result;
	
}

function deletePlace($conn, $place_id){
	// select berdasarkan id category
	$delete = $conn->query("DELETE FROM places WHERE id = '$place_id'");
	
	// kembalikan
	return $delete;
	
}

function updatePlace($conn, $request, $files){
	$place_id = $request['place_id'];
	// Cari data sebelumnya
	$find = $conn->query("SELECT * FROM places WHERE id = '$place_id'")->fetch_assoc();
	$old_photo = $find['photo']; // foto lama

	$category_id = $request['category_id'];
	$name = $request['name'];
	$latitude = $request['latitude'];
	$longitude = $request['longitude'];
	$street_address = $request['street_address'];
	$phone = $request['phone'];
	//$photo = $request['photo'];
	$type = $request['type'];

	if (isset($_FILES['photo'])) {
		$photo = $files['photo']['tmp_name']; //dapetin nama file
		if (file_exists($photo)) {
			$photo_name = $files['photo']['name']; //dapetin nama file
			$file_path = basename($files['photo']['name']);
			move_uploaded_file($photo, "../upload/" . $file_path);
		} else {
			// gunakan nama foto lama
			$photo_name = $old_photo;
		}
	} else {
		// gunakan nama foto lama
		$photo_name = $old_photo;
	}
	// select berdasarkan id category
	$query = $conn->query("UPDATE places SET category_id = '$category_id', 
											name = '$name', 
											latitude = '$latitude', 
											longitude = '$longitude', 
											street_address = '$street_address', 
											phone = '$phone', 
											photo = '$photo_name', 
											type = '$type'
							WHERE id = '$place_id'");
	// 
	$result = $query;
	// kembalikan
	return $result;
	
}

?>