<?php
require '../koneksi.php'; 
require '../function.php'; 

$result = getTypePlace($conn);

if (count($result) > 0) {
	$isSuccess = true;
	$data = $result;
} else {
	$isSuccess = false;
	$data = null;
}
header('Content-Type: application/json');
echo json_encode(compact('isSuccess', 'data'));