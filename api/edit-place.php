<?php
require '../koneksi.php'; 
require '../function.php';


$update = updatePlace($conn, $_POST, $_FILES);
$data = getDetailPlaceFromId($conn, $_POST['place_id']);
if ($update) {
	$msg = "Berhasil diupdate";
	$isSuccess = true;
} else {
	$msg = "Gagal mengupdate";
	$isSuccess = false;
}

$response = compact('isSuccess', 'msg', 'data');
	
// encode to jSon and print
header('Content-Type: application/json');
echo json_encode($response);