<?php 
require '../koneksi.php'; 
require '../function.php'; 
// get category id
$category_id = isset($_GET['category_id']) ? $_GET['category_id'] : null; // jika parameter ngga dibawa maka idnya 0

//return echo $;
//Get data dari database
$data = getPlaces($conn, $category_id);
$isSuccess = true;

// check if null
if (is_null($data)) {
	$data = null;
	$isSuccess = false;
}
//  asisiative array
$response = compact('isSuccess', 'data');
// encode to jSon and print
header('Content-Type: application/json');
echo json_encode($response);