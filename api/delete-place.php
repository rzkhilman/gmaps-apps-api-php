<?php
require '../koneksi.php'; 
require '../function.php'; 
$place_id = $_GET['place_id'];
$isDeleted = deletePlace($conn, $place_id);
if ($isDeleted) {
	$isSuccess = true;
	$msg = "Berhasil dihapus";
	$data = null;
} else {
	$isSuccess = false;
	$msg = "Gagal menghapus";
	$data = null;
}
header('Content-Type: application/json');
echo json_encode(compact('isSuccess', 'msg', 'data'));