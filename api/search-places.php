<?php
require '../koneksi.php'; 
require '../function.php';

// get category id
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : null; // jika parameter ngga dibawa maka idnya 0

// print($keyword);
// return;
//return echo $;
//Get data dari database
$data = searchPlace($conn, $keyword);
$isSuccess = true;

// check if null
if (is_null($data)) {
	$data = null;
	$isSuccess = false;
}
//  asisiative array
$response = compact('isSuccess', 'data');
// encode to jSon and print
header('Content-Type: application/json');
echo json_encode($response);