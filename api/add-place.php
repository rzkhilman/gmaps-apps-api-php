<?php
require '../koneksi.php'; 
require '../function.php';


$insert = insertPlace($conn, $_POST, $_FILES);

if ($insert) {
	$msg = "Berhasil ditambahkan";
	$isSuccess = true;
} else {
	$msg = "Gagal menambahkan";
	$isSuccess = false;
}

$response = compact('isSuccess', 'msg');
	
// encode to jSon and print
header('Content-Type: application/json');
echo json_encode($response);