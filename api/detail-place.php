<?php
require '../koneksi.php'; 
require '../function.php'; 
// parameter GET pembawa nilai lat dan long
$latitude = isset($_GET['latitude']) ? $_GET['latitude'] : 0;
$longitude = isset($_GET['longitude']) ? $_GET['longitude'] : 0;

$result = getDetailPlace($conn, $latitude, $longitude);
if (!empty($result)) {
	$isSuccess = true;
	$msg = "place dngan koordinat $latitude, $longitude ditemukan";
	$data = $result;
} else {
	$isSuccess = false;
	$msg = "place tidak di temukan dngan koordinat $latitude, $longitude";
	$data = null;
}
header('Content-Type: application/json');
echo json_encode(compact('isSuccess', 'msg', 'data'));