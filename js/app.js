var map;
// marker.setMap(null)
var markers = [];
var directionsService;
var directionsDisplay;
function initMap() {
    //direction
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;
    // Nah ini proses inisialisasi Mapnya.
    // dia menandai tag html dengan id MAP
    map = new google.maps.Map(document.getElementById('map'), {
        // bagian ini maksudnya, map akan berada di tengah2 dari koordinat yg kita tentukan dibawah
        // coba kita ganti koordinatnyaya
        center: {lat: -6.175110, lng: 106.865039}, // zoom ke jakarta
        zoom: 12
    });

    // Inisialisasi Directions
    initDirections();
    // gambar map
    //drawMap();

    //clearMarkers();
}
function drawMap(){

    // Buat latlong Bound untuk auto fit tampilan map
    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();
    // kosongkan variable markers
    markers = [];
    //looping membuat marker
    for (i = 0; i < locations.length; i++) {
        // kode untuk membuat marker
        var place_type = locations[i][3];
        switch(place_type){
        	case 'hotel':
        	var marker_icon = 'icon/marker_hotel.png';
        		break;
        	case 'restaurant':
        	var marker_icon = 'icon/marker_restaurant.png';
        		break;
        	case 'mall':
        	var marker_icon = 'icon/marker_mall.png';
        		break;
        	default:
        	var marker_icon = 'icon/marker_default.png';
        	break;
        }
        console.log(marker_icon);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            icon: marker_icon,
            map: map
        });
        // Nge-push marker ke Array Markers
        markers.push(marker);
        console.log(marker);
        // extend koordinat ke bound variable
        bounds.extend(marker.position);

        // ketika marker di klik
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {

            	var contentString = '<div>'+
                          '<h1>' + locations[i][0] + '</h1>'+
                          '<div>'+
                          '<img src="upload/' + locations[i][6] +  '" width="250px"/>' +
                          '<p><b>' + locations[i][4] + '</b></p>'+
                          '<br><p><b>' + locations[i][5] + '</b></p>'+
                          '</div>'+
                          '</div>';
                //mengambil nama, alamat, foto dan telp di maker
                infowindow.setContent(contentString);
                infowindow.open(map, marker);

                // tampilkan di log
                console.log(locations[i][5]);
            }
        })(marker, i));

    }
    // Zoom tampilan berada di tengah2 semua marker
    map.fitBounds(bounds);

}

function clearMarkers(){
	for (var i = 0; i < markers.length; i++) {
	    markers[i].setMap(null);
	  }
}

// BAGIAN DIRECTIONS

function initDirections(){

// set direction ke map
    directionsDisplay.setMap(map);
    // 
    var onChangeHandler = function() {
    	

        calculateAndDisplayRoute(directionsService, directionsDisplay);
    };
    document.getElementById('start').addEventListener('change', onChangeHandler);
    document.getElementById('end').addEventListener('change', onChangeHandler);

}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    directionsService.route({
        origin: $('#start').val(),
        destination: $('#end').val(),
        travelMode: 'DRIVING'
        //DRIVING (Default) indicates standard driving directions using the road network.
        //BICYCLING requests bicycling directions via bicycle paths & preferred streets.
        //TRANSIT requests directions via public transit routes.
        //WALKING requests walking directions via pedestrian paths & sidewalks.

    }, function(response, status) {
        // tampung ke variable
        legs = response.routes[0].legs[0];
        duration = legs.duration;
        distance = legs.distance;

        // console.log(legs);
        // console.log(duration.text);
        // console.log(distance.text);
        $('#duration').text("Waktu: " + duration.text);
        $('#distance').text("Jarak: " + distance.text);
        if (status == "OK") {
        	// inisialisasi directiosnnya dulu
	    	initDirections();
        	clearMarkers();
            directionsDisplay.setDirections(response);
        } else {
            alert("Request rute gagal " + status);
        }
    });
}


function showPlace(loc_id){
     var infowindow = new google.maps.InfoWindow();
    clearMarkers();
	// kode untuk membuat marker
        var place_type = locations[loc_id][3];
        switch(place_type){
            case 'hotel':
            var marker_icon = 'icon/marker_hotel.png';
                break;
            case 'restaurant':
            var marker_icon = 'icon/marker_restaurant.png';
                break;
            case 'mall':
            var marker_icon = 'icon/marker_mall.png';
                break;
            default:
            var marker_icon = 'icon/marker_default.png';
            break;
        }
        
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[loc_id][1], locations[loc_id][2]),
            icon: marker_icon,
            map: map
        });
        // Nge-push marker ke Array Markers
        markers.push(marker);
        
        map.setZoom(17);
        map.panTo(marker.position);
        // ketika marker di klik
        google.maps.event.addListener(marker, 'click', (function(marker, loc_id) {
            return function() {

                var contentString = '<div>'+
                          '<h1>' + locations[loc_id][0] + '</h1>'+
                          '<div>'+
                          '<img src="upload/' + locations[loc_id][6] +  '" width="250px"/>' +
                          '<p><b>' + locations[loc_id][4] + '</b></p>'+
                          '<br><p><b>' + locations[loc_id][5] + '</b></p>'+
                          '</div>'+
                          '</div>';
                //mengambil nama, alamat, foto dan telp di maker
                infowindow.setContent(contentString);
                infowindow.open(map, marker);

                // tampilkan di log
                console.log(locations[loc_id][5]);
            }
        })(marker, loc_id));

}