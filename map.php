<?php 
	include 'koneksi.php';
	include 'function.php';
	
//	print_r(getCategory($conn));
	//return;
	
	if(isset($_GET['category_id'])){
		$category_id = $_GET['category_id'];
	} else {
		$category_id = '0';
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <!-- This stylesheet contains specific styles for displaying the map
         on this page. Replace it with your own styles as described in the
         documentation:
         https://developers.google.com/maps/documentation/javascript/tutorial -->
     <link rel="stylesheet" href="style.css"/>
	<style>
	html,body {
		height: 100%;
		margin:0;
		padding:0;
	}
	#map {
	height:100%;
	}
	</style>
  </head>
  <body>
<!--  	disini nanti map akan ditampilkan-->
    <div id="map"></div>
    
<!--    MENU KANAN-->
	<div class="floating-panel">
		<h4>Kategori Tempat</h4>
        <select class="lebarCategory" id="category">
        	<option value="0">Semua</option>
        <?php
		$place_category = getCategory($conn);
		foreach($place_category as $value){
			$auto_select = autoSelect($category_id, $value[id]);
			
			echo "<option value='$value[id]' $auto_select>$value[title]</option>";
		}
		?>
         </select><br><br>
        <input type="text" name="cari" id="cari" class="lebarCategory" placeholder="Cari..."><br><br>
        Jarak :
      <h4>Tempat</h4>
         <ul>
         	<div id="place_list"></div>
            <?php
				$places = getPlaces($conn, $category_id);

				// if(is_null($places)){
				// 	echo "<option value=''>-- kosong --</option>";
				// } else {
				// 	// looping
				// 	foreach($places as $place){
				// 		echo "<li>$place[name]</li>";
				// 	}
				// }
			?>
         </ul>
         </div>

         <!-- PANEL KIRI -->
         <div class="floating-panel left">
         	<b>Start: </b>
				    <select id="start">
				      <?php
					  	if(!is_null($places)){
							// looping
							foreach($places as $place){
								// print array buat javascript
								echo "<option value='$place[latitude],$place[longitude]'>$place[name]</option>";

							}
						}
					  ?>
				    </select>
				    <b>End: </b>
				    <br>
				    <select id="end">
				    <?php
					  	if(!is_null($places)){
							// looping
							foreach($places as $place){
								// print array buat javascript
								echo "<option value='$place[latitude],$place[longitude]'>$place[name]</option>";

							}
						}
					  ?>				     
				    </select>

				    <br>
				    <span id="distance">Jarak : 0</span>
				    <br>
				    <span id="duration">Waktu : 0</span>
         </div>

	<script>
    var locations = [
      //['North Jakarta', -6.138377, 106.866466],
	  <?php
	  	if(!is_null($places)){
			// looping
			foreach($places as $place){
				// print array buat javascript
				echo "['$place[name]', $place[latitude], $place[longitude], '$place[type]', '$place[photo]', '$place[street_address]', '$place[phone]'],";

			}
		}
	  ?>
    ];

    
    // bagian direction
    var legs = null;
    var duration = null;
    var distance = null;
    </script>
    <script src="js/app.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQWti4OcdJItjY_w8yJHULMGCgO-Nq8_Y&callback=initMap"
    async defer></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
		$(document).ready(function(){
			// event onChange
			$('#category').change(function(event){
				// clear markersnya dulu
				clearMarkers();

				var category_id = $(this).val();
				
				// redirect ke map.php dngan bawa parameter get
				//window.location.href = 'map.php?category_id=' + category_id;
				var api_url = "api/get-places.php?category_id=" + category_id;
				$.get(api_url, function( data ) {
				  	var places = data.data;
				  	// kosongkan element dalam tag div id place_list
				    $('#place_list').html('');

				    //console.log(data);
				    // kosongkan dulu variable locaitons
				    locations = [];
				    // cek jika status sukses
				    if (data.isSuccess) {
				    	// looping data places
					  	$.each(places, function(index, place) {
						  	var html_dom = "<li><a href='#' onClick='showPlace("+ index +")'>" + place.name + "</a></li>";
					        console.log(place.type);
					        // append ke div
					        $('#place_list').append(html_dom);
					        // push data ke array locations
					        locations.push([place.name, place.latitude, place.longitude, place.type, place.street_address, place.phone, place.photo]);
					    });
				    } else {
				    	// jika nilai isSuccessnya false
				    	$('#place_list').append('<li> -- kosong -- </li>');
				    }

				    //console.log(locations);
				  	// gambar ulang peta
				directionsDisplay.setMap(null);
				drawMap();

				});
			});


			//
			$('#cari').keypress(function(event){

			  	if(event.which == 13) {
			  	// tampilin pesan di log 

			  	//kosongkan marker
			  	clearMarkers();

				console.log("cari");
				var keyword = $(this).val();
				
				// redirect ke map.php dngan bawa parameter get
				//window.location.href = 'map.php?category_id=' + category_id;
				var api_url = "api/search-places.php?keyword=" + keyword;
				$.get(api_url, function( data ) {
				  	var places = data.data;
				  	// kosongkan element dalam tag div id place_list
				    $('#place_list').html('');

				    //console.log(data);
				    // kosongkan dulu variable locaitons
				    locations = [];
				    // cek jika status sukses
				    if (data.isSuccess) {
				    	// looping data places
					  	$.each(places, function(index, place) {
						  	var html_dom = "<li><a href='#' onClick='showPlace("+ index +")'>" + place.name + "</a></li>";
					        console.log(place.type);
					        // append ke div
					        $('#place_list').append(html_dom);
					        // push data ke array locations
					        locations.push([place.name, place.latitude, place.longitude, place.type, place.street_address, place.phone, place.photo]);
					    });
				    } else {
				    	// jika nilai isSuccessnya false
				    	$('#place_list').append('<li> -- kosong -- </li>');
				    }

				    //console.log(locations);
				  	// Jalankan funtion initMap lagi, gunanya untuk merefresh Map
				  	directionsDisplay.setMap(null);
				  	drawMap();
				});    
		        }
				
			});
		});
	</script>
  </body>
</html>